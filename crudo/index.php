<?php
include_once 'vendor/autoload.php';

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN']))
{
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache para 1 dia
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
	{
        header("Access-Control-Allow-Methods: GET");
	}
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
	{
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	}
    exit(0);
}

header('Content-Type: application/json');

$resp = array();
$field_permitidos = array('id','cover','name','first_name','last_name','age_range','link','gender','locale','picture','timezone','updated_time','verified');

//validacion de metodo utilizado
if(!isset($_GET))
{
	$resp = array(
		'status'	=> 99,
		'value'		=> 'Debe enviar los parametros por GET.'
		);
	die(json_encode($resp));
}

//validacion parametro iduser
if(!isset($_GET['iduser']))
{
	$resp = array(
		'status'	=> 98,
		'value'		=> 'El parametro iduser es obligatorio.'
		);
	die(json_encode($resp));
}

$iduser = $_GET['iduser'];

if(!ctype_digit($iduser))
{
	$resp = array(
		'status'	=> 97,
		'value'		=> 'El parametro iduser debe ser numerico.'
		);
	die(json_encode($resp));
}

if($iduser == '')
{
	$resp = array(
		'status'	=> 96,
		'value'		=> 'El parametro iduser no debe estar vacio.'
		);
	die(json_encode($resp));
}

//Objeto facebook con opciones de app aivo test
$fb = new Facebook\Facebook([
	'app_id'				=> '1592623234129704',
	'app_secret'			=> 'b35512e1daa13314fde2f4bf3b7c7317',
	'default_graph_version' => 'v2.8',
	'default_access_token'	=> '1592623234129704|E1dec5UJMH4H_t6-0lG_8WXOmjs'
	]);

//324659181282654

//parseo de filds a obtener
if(isset($_GET['fields']) && $_GET['fields'] != '')
{
	$arr_fields_params = array();
	$arr_fields_request = explode(',',$_GET['fields']);
	foreach($arr_fields_request as $field_request)
	{
		if(in_array(trim($field_request), $field_permitidos))
		{
			if(!in_array(trim($field_request), $arr_fields_params))
			{
				$arr_fields_params[] = trim($field_request);
			}
		}
	}
}


if(count($arr_fields_params) > 0)
{
	$fields = implode(',' , $arr_fields_params);
}
else
{
	$fields = implode(',' , $field_permitidos);
}


//consulta api Facebook
try
{
	$perfil_facebook = $fb->get('/'.$iduser.'?fields='.$fields);
}
catch(Exception $e)
{
	$resp = array(
		'status'	=> $e->getCode(),
		'value'		=> $e->getMessage());
	die(json_encode($resp));
}

//si todo fue ok devuelvo el perfil encontrado
die(json_encode(array(
			'status'	=> 0,
			'value'		=> json_decode($perfil_facebook->getBody())
			)
		)
	);


?>